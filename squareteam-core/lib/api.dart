library api;

import "dart:html";
import "dart:async";
import 'dart:convert';

import 'typed_data/utils.dart';

import 'package:dart_pbkdf2/pbkdf2.dart';
import 'package:crypto/crypto.dart';
import 'package:fixnum/fixnum.dart';
import 'package:json/json.dart';
import 'package:dart_config/default_browser.dart';
import 'package:utf/utf.dart';
import 'package:logging/logging.dart';


part "api/api_exception.dart";
part "api/api_auth.dart";
part "api/api_request.dart";

final Logger log = new Logger('Api');

class Api {

   final ApiAuth auth;

   // 'const constructors' make Singletons
   //  this means that there is only one instance per auth
   const Api([ApiAuth this.auth]);

   // Public functions (API)

   Future read(String url, { bool secure : true, data }) {
      log.finer('Api.read("${url}")');
      return _prepare(url, "GET", data : data, secure : secure).execute();
   }

   Future create(String url, { bool secure : true, data }) {
      log.finer('Api.create("${url}")');
      return _prepare(url, "POST", data : data, secure : secure).execute();
   }

   Future update(String url, { bool secure : true, data }) {
      log.finer('Api.update("${url}")');
      return _prepare(url, "PUT", data : data, secure : secure).execute();
   }

   Future destroy(String url, { bool secure : true, data }) {
      log.finer('Api.destroy("${url}")');
      return _prepare(url, "DELETE", data : data, secure : secure).execute();
   }

   // Private functions

   ApiRequest _prepare(String url, String method, { bool secure : true, data }) {
      if (secure && auth != null) {
         return new ApiRequestSecure(url, auth,  method : method, data : data );
      } else {
         return new ApiRequest(url,  method : method, data : data );
      }
   }

}