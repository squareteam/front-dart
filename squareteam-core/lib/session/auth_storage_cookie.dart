part of session;

class AuthStorageCookie extends AuthStorage {

   bool empty() => cookie.get(AuthStorage.name) == null;

   ApiAuth retrieve() {
      if (!empty()) {
         log.finer('retrieve session from cookie');
         String value = cookie.get(AuthStorage.name);

         List<String> values = value.split(':');

         if (values.length == 2) {
            return new ApiAuth(values[1], values[0]);
         } else {
            log.warning('stored session malformed (cookie)');
            return null;
         }

      } else {
         return null;
      }
   }

   void store(ApiAuth auth) {
      log.finer('store session in cookie');
      String value = auth.identifier + ":" + auth.token;
      cookie.set(AuthStorage.name, value, path: '/');
   }

   void destroy() {
      log.finer('delete session from cookie');
      cookie.remove(AuthStorage.name, path: '/');
   }

}
