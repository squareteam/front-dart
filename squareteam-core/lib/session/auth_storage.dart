part of session;

var config = loadConfigSync();

abstract class AuthStorage {

   static final String name = config["session_name"];

   static bool capable() => true;

   bool empty();

   ApiAuth retrieve();

   void store(ApiAuth auth);

   void destroy();

}
