part of session;

class AuthStorageLocalStorage extends AuthStorage {

   static bool capable() => window.localStorage != null;

   bool empty() =>  !window.localStorage.containsKey(AuthStorage.name);

   ApiAuth retrieve() {

      if (!empty()) {
         log.finer('retrieve session from localStorage');
         String value = window.localStorage[AuthStorage.name];

         List<String>  values = value.split(':');

         if (values.length == 2) {
            return new ApiAuth(values[1], values[0]);
         } else {
            log.warning('stored session malformed (localStorage)');
            return null;
         }

      } else {
         return null;
      }
   }

   void store(ApiAuth auth) {
      log.finer('store session in localStorage');
      String value = auth.identifier + ":" + auth.token;
      window.localStorage[AuthStorage.name] = value;
   }

   void destroy() {
      log.finer('delete session from localStorage');
      window.localStorage.remove(AuthStorage.name);
   }

}
