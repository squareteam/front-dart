part of session;

class SessionException implements Exception {
   final message;

   const SessionException(String _message) : message = _message;

   String toString() => "SessionException : $message";
}
