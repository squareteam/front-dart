part of session;

class AuthStorageMemory extends AuthStorage {

   ApiAuth _inst;

   bool empty() => _inst != null;

   ApiAuth retrieve() {

      if (!empty()) {
      log.finer('retrieve session from memory');
         return _inst;

      } else {
         log.severe('stored session malformed (memory)');
         return null;
      }
   }

   void store(ApiAuth auth) {
      log.finer('store session in memory');
      _inst = auth;
   }

   void destroy() {
      log.finer('delete session from memory');
      _inst = null;
   }

}
