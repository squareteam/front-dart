library session;

import "api.dart";
import "api/utils.dart";
import 'typed_data/utils.dart';


import 'dart:async';
import 'dart:html';
import 'dart:typed_data';

import 'package:dart_pbkdf2/pbkdf2.dart';
import 'package:crypto/crypto.dart';
import 'package:cookie/cookie.dart' as cookie;
import 'package:dart_config/default_browser.dart';
import 'package:utf/utf.dart';
import 'package:logging/logging.dart';





part "session/session_exception.dart";
part "session/auth_storage.dart";
part "session/auth_storage_cookie.dart";
part "session/auth_storage_memory.dart";
part "session/auth_storage_localstorage.dart";

final Logger log = new Logger('Session');

// TODO : code needs to be cleaned a little..

class Session {

   AuthStorage   provider = new AuthStorageMemory();

   /* User */var     user;
   ApiAuth           auth;


   Session(ApiAuth this.auth, /*User*/ this.user, [AuthStorage this.provider]);

   void destroy() {
      log.finest('Destroy provider');
      provider.destroy();
   }

   Future logout() {
      Completer   _completer = new Completer();
      Api         _apiAuthenticated = new Api(auth);

      _apiAuthenticated.update('/logout').then( (noop) {
         log.finer('logout from api');
         this.destroy();
         _completer.complete();
      }).catchError( (e) {
         log.severe("fails to logout from api");
         _completer.completeError(e);
      });

      return _completer.future;
   }

   bool isAnonymous()      => user == null;

   bool isAuthenticated()  => auth != null;

   bool save() {
      if (auth == null) {
         log.warning('unable to save session, no auth provided');
         return false; // return Exception ? throw ?
      } else {
         log.finer('Session.save()');
         provider.store(auth);
         return true;
      }
   }

   // Function to generate user session token
   //    TODO : cleanup and optimize algos
   static String generateToken(String password, String salt1, String salt2) {
      log.finer('Compute user token, hang tight...');
      window.console.time('token generation');
      String      _pbkdf2;
      HMAC        token;
      var         pbkdf2         = new Pbkdf2();
      List<int>   password_bytes = encodeUtf8(password);
      List<int>   salt1_bytes    = hexStringToBytes(salt1);

      window.console.time('pbkdf2 generation');
      _pbkdf2 = pbkdf2.generate(password_bytes, salt1_bytes, 1000, 32);
      window.console.timeEnd('pbkdf2 generation');

      token = new HMAC(new SHA256(), concatBytes(hexStringToBytes(salt2), hexStringToBytes(_pbkdf2)) );

      token.add(encodeUtf8(login));

      var __res = CryptoUtils.bytesToHex(token.close());

      window.console.timeEnd('token generation');

      return __res;
   }

   static Future login(String login, String password, [bool trusted_browser = true]) {
      Completer _completer       = new Completer();
      Api       _apiAnonymous    = new Api();

      AuthStorage _provider;

      log.finer('Session.login()');

      // Configure provider
      if (trusted_browser) {
         if (AuthStorageLocalStorage.capable()) {
            _provider = new AuthStorageLocalStorage();
         } else {
            _provider = new AuthStorageCookie();
         }
      } else {
         _provider = new AuthStorageMemory();
      }

      //////////////
      // 1. Login //
      //////////////

      log.finer('Step 1, fetch salts');

      _apiAnonymous.update('/login', data : { "identifier" : login }, secure : false).then( (response1) {

         if (!reponseMatchFormat(response1, ["salt1", "salt2"])) {
            log.warning('Failed to fetch salts from server response : $response1');
            _completer.completeError(new SessionException('login.failed'));
         } else {
            log.finer('Salts fetched with success');
            var   auth              = new ApiAuth(Session.generateToken(password, response1["salt1"], response1["salt2"]), login);
            Api   _apiAuthenticated = new Api(auth);

            ///////////////////////////////
            // 2. check session validity //
            ///////////////////////////////

            log.finer('Step 2, check session');

            _apiAuthenticated.read('/user/me').then( (response2) {
               log.finer('Session valid');
               _completer.complete(new Session(auth, response2, _provider));
            }).catchError( (e) {
               log.warning('Session invalid ! Due to $e');
               _completer.completeError(new SessionException('session.failed'));
            });
         }

      }).catchError( (e) {
         // identify error
         log.warning('Fails to fetch salts due to $e');
         _completer.completeError(new SessionException('login.failed'));
      });


      return _completer.future;
   }

   static Future restore() {
      Completer _completer = new Completer();

      log.finer('Session.restore()');

      AuthStorage _provider;

      log.finer("Try to find session in storages");

      if (AuthStorageLocalStorage.capable()) {
         log.finest('Try to restore from LocalStorage');
         _provider = new AuthStorageLocalStorage();

         if (_provider.empty()) {
            log.finest('LocalStorage is empty');
            _provider = null;
         }
      } else {
         _provider = new AuthStorageCookie();
         log.finest('Try to restore from Cookies');

         if (_provider.empty()) {
            log.finest('Cookie is empty');
            _provider = new AuthStorageMemory();

            log.finest('Try to restore from Memory');
            if (_provider.empty()) {
               log.finest('Memory is empty');
               _provider = null;
            }

         }
      }


      // nothing to restore
      if (_provider == null) {
         log.warning('Session not found');
         _completer.completeError(new SessionException('restore.fail'));
      } else {
         ApiAuth auth              = _provider.retrieve();

         if (auth == null) {
            _completer.completeError(new SessionException('restore.malformed')); // replace by FormatException ?
         }

         Api     _apiAuthenticated = new Api(auth);

         ////////////////////////////
         // check session validity //
         ////////////////////////////
         _apiAuthenticated.read('/user/me').then( (response) {
            _completer.complete(new Session(auth, response, _provider));
         }).catchError( (e) {
            _completer.completeError(new SessionException('restore.session.fail'));
         });
      }

      return _completer.future;
   }

}
