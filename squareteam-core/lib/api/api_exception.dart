part of api;

// const constructor allow to limit memory allocation

class ApiException implements Exception {

   // api error message
   final String message;

   // api error type
   final String type;

   // instance are unique for each type/message couple
   const ApiException(this.type, [this.message = ""]);

   String getMessage() => message;

   String toString() => "ApiException($type): $message";
}


class HttpException implements Exception {
   final String message;

   // instance are unique for each message
   const HttpException(this.message);

   String toString() => "HttpException: $message";
}
