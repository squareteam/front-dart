part of api;

var config = loadConfigSync();

/**
 * ApiRequest class
 *    Represent a XHR request
 *
 * Wrap response and handle Squareteam secured protocol (see doc/api/README)
 *
 * Return `Future`.
 */
class ApiRequest {

   String _method;

   // Request path
   String _url;

   // Request base URL
   String base = config["api_url"];

   // Request data
   Map<String, String> _data = {};

   // request completer
   Completer _completer = new Completer();

   HttpRequest req;



   // Constructor

   ApiRequest(String url, { String method : "POST", data }) {
      _url = url;
      this.method = method;
      if (data != null) {
         _data  = data;
      }
   }

   // Public methods

   void set method(String m) {
      List<String> methods = ["GET", "DELETE", "PUT", "POST"];
      if (methods.indexOf(m) == -1) {
         throw new StateError("Method not allowed : " + m);
      } else {
         _method = m;
      }
   }

   String get url => base + _url;

   // Allow to override data format
   Map<String, String> get data => _data;

   // Allow to add headers
   Map<String, String> get headers => {};


   /**
    * Execute a request asynchronously and return a `Future`
    */
   Future execute() {

      req = new HttpRequest();

      req.open(_method, url);

      // API returns JSON text
      req.responseType = "application/json";

      // Handle request errors
      req.onError.listen(_handleError);
      req.onTimeout.listen(_handleError);
      // Handle finished request
      req.onLoadEnd.listen(_handleResponse);

      // to improve
      headers.forEach((String key, String value) {
         req.setRequestHeader(key, value);
      });

      FormData form_data = new FormData();

      data.forEach((String key, String value) {
         form_data.append(key, value);
      });

      req.send(form_data);

      return _completer.future;

   }

   // Private methods

   void _handleResponse(ProgressEvent evt) {
      var response;

      log.fine("[$_method][$_url] RESPONSE: ${req.responseText}");

      try {
         response = JSON.decode(req.responseText);
      } on FormatException catch(e) {
         this._completer.completeError( new ApiException("response.parse_error", e) );
         return;
      }

      if (response["error"].length > 0) {
        this._completer.completeError( new ApiException("response.error", response["error"]) );
      } else if (!response.containsKey("data")) {
        this._completer.completeError( new ApiException("response.malformed") );
      } else {
        this._completer.complete( response["data"] );
      }
   }

   void _handleError(ProgressEvent evt) {
      // TODO
      log.warning("[$_method][$_url] ERROR (${req.status}): ${req.responseText}");
      // if (e.status && e.status != 500){
      //    this._completer.completeError( new HttpException(e.status) );
      // } else { // data violation return 500 errors
      //    _handleSuccess(e.responseText);
      // }
   }

}


class ApiRequestSecure extends ApiRequest {

   // a request is valid for 2 minutes
   int timestamp;

   ApiAuth auth;

   // ApiRequestSecure take a ApiAuth parameters and call ApiRequest constructor
   ApiRequestSecure(String url, this.auth, { String method : "POST", data }) : super(url, method : method, data : data) {}

   /**
    * Squareteam secure protocol custom headers
    */
   Map<String, String> get headers {
      List<String>         blob     = [];
      Map<String, String>  _headers = {};
      HMAC                 hmac     = new HMAC(new SHA256(), hexStringToBytes(auth.token));

      log.info('Generating headers for secure request');

      timestamp = ((new DateTime.now()).millisecondsSinceEpoch / 1000).round();

      List<String> _data_keys = _data.keys.toList();
      _data_keys.sort();

      for (var key in _data_keys) {
         blob.add( " ${Uri.encodeComponent(key)}=${Uri.encodeComponent(_data[key])}" );
      }

      _headers["St-Timestamp"]   = timestamp.toString();
      _headers["St-Identifier"]  = auth.identifier;

      hmac.add( encodeUtf8(_method                    + ":" ) );
      hmac.add( encodeUtf8(_url                       + ":" ) );
      hmac.add( encodeUtf8(_headers["St-Timestamp"]   + ":" ) );
      hmac.add( encodeUtf8(                   blob.join("&")) );

      _headers["St-Hash"] = CryptoUtils.bytesToHex(hmac.close());

      log.info('HASH : ' + _headers["St-Hash"]);

      return _headers;
   }

}
