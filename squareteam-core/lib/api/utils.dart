import 'package:logging/logging.dart';

final Logger log = new Logger('Api utils');


/**
 * Allow to check that a reponse contains wanted keys
 */
bool reponseMatchFormat(response, List<String> fields_expected, { bool no_null : true }) {
   if (response == null || !(response is Map<String, dynamic>)) {
      log.warning("response is null or not a map");
      return false;
   }

   return fields_expected.every( (field) {
      // TODO : simplify..

      if (!response.containsKey(field)) {
         log.warning("key $field is missing in response");
         return false;
      }

      if (!no_null && response[field] == null) {
         log.warning("key $field is null in response");
         return false;
      } else {
         log.finest("key $field found in response");
         return true;
      }

   });

}
