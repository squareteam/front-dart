part of api;

/**
 * Represent an Auth
 *    Every auth is unique for each Api instance
 */
class ApiAuth {
   // User login (email)
   String _identifier;
   // User token (String hex)
   String _token;

   ApiAuth(this._token, this._identifier);

   String get token        => _token;
   String get identifier   => _identifier;
}
