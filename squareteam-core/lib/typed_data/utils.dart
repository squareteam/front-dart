import 'dart:typed_data';
// import 'dart:html';



/**
 * Convert hex strings to bytes
 *    from https://github.com/Daegalus/dart-base32
 */
final RegExp hexStringToBytes_regex = new RegExp('[0-9a-f]{2}');
Uint8List hexStringToBytes(hex){
   // window.console.time('hexStringToBytes_"$hex"');
   int i = 0;
   Uint8List bytes = new Uint8List(hex.length~/2);
   for(Match match in hexStringToBytes_regex.allMatches(hex.toLowerCase())) {
      bytes[i++] = int.parse(hex.toLowerCase().substring(match.start,match.end),radix:16);
   }
   // window.console.timeEnd('hexStringToBytes_"$hex"');
   return bytes;
}

/**
 * Concat two bytes
 *    TODO :   Can be done asynchronously with Workers
 *             cause the size of known
 */
Uint8List concatBytes(Uint8List a, Uint8List b) {
   // window.console.time("concatBytes");
   var concat_key    = new Uint8List(a.length + b.length);
   var concat_key_it = 0;

   // concat bytes by bytes -> a + b
   a.forEach( (byte) {
      concat_key[concat_key_it++] = byte;
   });
   b.forEach( (byte) {
      concat_key[concat_key_it++] = byte;
   });
   // window.console.timeEnd("concatBytes");
   return concat_key;
}
