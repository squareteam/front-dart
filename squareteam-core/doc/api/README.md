# API

This library allow to communicate with Squareteam API server

This library contains many parts : 

* Api
* ApiAuth
* ApiRequest
* ApiRequestSecure
* ApiException
* HttpException


## Request

There is two types of API requests :

* Secure requests

   1) Client must compute data into a bunch (sorted by key):

       BLOB = "key1=value1&key2=value2&key3=value3..."

   2) Client generate a TIMESTAMP

   3) Client compute using his token:

       HASH = HMAC_SHA256(TOKEN,
                          HTTP_Method + ":" + URL_PATH + ":" + TIMESTAMP + ":" + BLOB)

   4) Client add three field in the request HTTP header:

       - "St-Identifier": user_identifier
       - "St-Timestamp" : TIMESTAMP
       - "St-Hash"      : HASH

   5) Client send data (in the same order than blob) with the custom HTTP header.

* Plain requests (only for `/register` and `/login` [and few more])