import 'package:unittest/unittest.dart';
import 'package:unittest/html_config.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:logging/logging.dart';
import 'dart:html';
import 'package:st_core/session.dart';
import 'package:st_core/api.dart';


void start_test() {
  Session _test_session;

  test('Try to login with test account (test@test.fr)', () {
    Session.login("test@test.fr", "test", true).then( expectAsync1( (session) {
      logMessage('success, store session');
      window.console.dir(session);
      session.save();
    })).catchError( protectAsync1( (e) {
      window.console.error(e);
      throw new Exception("Failed to login...");
    }));
  });

  test('Try to restore session from localstorage', () {
    Session.restore().then( expectAsync1( (session) {
      logMessage('success, session retrieved');
      window.console.dir(session);
      _test_session = session;
    })).catchError( protectAsync1( (e) {
      window.console.error(e);
      throw new Exception("Failed to restore session...");
    }));
  });

  test('Try to logout', () {
    expect(_test_session, isNotNull);
    _test_session.logout().then( expectAsync1( (e) {
      logMessage('logout succeed');
      expect(window.localStorage.containsKey('ST_SESSION'), isFalse);
    })).catchError( protectAsync1( (e) {
      window.console.error(e);
      throw new Exception("Failed to logout...");
    }));
  });

  test('Try to restore session from localstorage should fails', () {
    Session.restore().then( protectAsync1( (session) {
      throw new Exception("Restore should not succeed !");
    })).catchError( expectAsync1( (e) {
      
    }));
  });
}


main() {

  Logger.root.level = Level.ALL; // print all logs
  Logger.root.onRecord.listen((LogRecord rec) {
    print('[${rec.loggerName}]: ${rec.message}');
  });

  useHtmlConfiguration();

  var anonymousApi = new Api();

  Map<String, String> account = { "identifier" : "test@test.fr", "password" : "test" };

  anonymousApi.create('/register', data : account, secure : false).then( (result) {
    start_test();
  }).catchError( (e) {
    if (e is ApiException && e.getMessage() == "api.email_violation") {
      // test FTW, continue
      start_test();
    } else {
      throw new Exception('Failed to create test account due to an $e');
    }
  });

}
