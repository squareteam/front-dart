import 'package:unittest/unittest.dart';
import 'package:unittest/html_config.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:logging/logging.dart';
import 'package:st_core/api.dart';

import 'dart:html';


main() {

  Logger.root.level = Level.ALL; // print all logs
  Logger.root.onRecord.listen((LogRecord rec) {
    print('[${rec.loggerName}]: ${rec.message}');
  });

  useHtmlConfiguration();

  var anonymousApi = new Api();

  test('API GET /', () {
    anonymousApi.read('/', secure : false)
                .then( protectAsync1( () {}) )
                .catchError( expectAsync1( (e) {
                  expect(e is ApiException, isTrue);
                }));
  });

  test('API PUT', () {
    anonymousApi.update('/', secure : false)
                .then( protectAsync1( () {}) )
                .catchError( expectAsync1( (e) {
                  expect(e is ApiException, isTrue);
                }));
  });

  test('API POST', () {
    anonymousApi.create('/', secure : false)
                .then( protectAsync1( () {}) )
                .catchError( expectAsync1( (e) {
                  expect(e is ApiException, isTrue);
                }));
  });

  test('API DELETE', () {
    anonymousApi.destroy('/', secure : false)
                .then( protectAsync1( () {}) )
                .catchError( expectAsync1( (e) {
                  expect(e is ApiException, isTrue);
                }));
  });

}